class AddRoleToInvites < ActiveRecord::Migration[6.0]
  def self.up
    change_table :invites do |t|
      t.string :role, :default => "guest"
      t.string :invite_key
    end
  end

  def self.down
    remove_column :invites, :role
    remove_column :invites, :invite_key
  end
end
