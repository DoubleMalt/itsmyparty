class AddInviterToInvites < ActiveRecord::Migration[6.0]
  def self.up
    change_table :invites do |t|
      t.string :inviter
    end
    Invite.all do |invite|
      invite.inviter = invite.party.creator_mail
      invite.save
    end
  end

  def self.down
    remove_column :invites, :inviter
  end
end
