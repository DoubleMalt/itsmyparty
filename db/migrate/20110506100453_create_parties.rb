class CreateParties < ActiveRecord::Migration[6.0]
  def self.up
    create_table :parties do |t|
      t.string :location
      t.string :topic
      t.string :description
      t.integer :recursive_invitation_depth
      t.datetime :start
      t.datetime :end
      t.integer :invite_count
      t.string :creator_mail

      t.timestamps
    end
  end

  def self.down
    drop_table :parties
  end
end
