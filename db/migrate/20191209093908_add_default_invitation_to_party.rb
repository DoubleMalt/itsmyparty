class AddDefaultInvitationToParty < ActiveRecord::Migration[6.0]
  def change
    add_column :parties, :default_invitation, :text
  end
end
