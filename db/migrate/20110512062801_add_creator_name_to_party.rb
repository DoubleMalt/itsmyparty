class AddCreatorNameToParty < ActiveRecord::Migration[6.0]
  def self.up
    change_table :parties do |t|
      t.string :creator_name
    end
  end

  def self.down

    remove_column :parties, :creator_name
  end
end
