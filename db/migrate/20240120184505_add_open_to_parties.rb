class AddOpenToParties < ActiveRecord::Migration[6.0]
  def change
    add_column :parties, :open, :boolean
  end
end
