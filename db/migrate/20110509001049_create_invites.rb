class CreateInvites < ActiveRecord::Migration[6.0]
  def self.up
    create_table :invites do |t|
      t.string :name
      t.string :email
      t.string :custom_invitation
      t.string :status
      t.references :party

      t.timestamps
    end
  end

  def self.down
    drop_table :invites
  end
end
