class CommentsController < ApplicationController
  load_and_authorize_resource :party
  load_and_authorize_resource :invite, :through => :party
  load_and_authorize_resource :comment, :through => :invite

  def create
    @comment.invite = @invite

    respond_to do |format|
      if @comment.save
        format.html { redirect_to party_invite_path(@invite.party, @invite) }
        format.json  { render :json => @comment, :status => :created, :location => @invite }
      else
        format.html { render :action => "new" }
        format.json  { render :json => @invite.errors, :status => :unprocessable_entity }
      end
    end
  end

  private

  def comment_params
    params[:comment].permit(:text)
  end
end
