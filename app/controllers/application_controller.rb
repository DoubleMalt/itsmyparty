class ApplicationController < ActionController::Base
  protect_from_forgery
  def default_url_options(options={})
    options.merge(:key => params[:key])
  end
  
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :alert => exception.message # FIXME
  end

  private

  def current_ability
    @current_ability ||= Ability.new(request.params[:key])
  end
  
end
