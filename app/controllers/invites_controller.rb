class InvitesController < ApplicationController
  load_and_authorize_resource :party
  load_and_authorize_resource :invite, :through => :party
  # GET /parties/invites
  # GET /parties/invites.json
  def index

    respond_to do |format|
      format.html # index.html.erb
      format.json  { render :json => @invites }
    end
  end

  # GET /parties/invites/1
  # GET /parties/invites/1.json
  def show

    respond_to do |format|
      format.html # show.html.erb
      format.json  { render :json => @invite }
    end
  end

  # GET /parties/invites/new
  # GET /parties/invites/new.json
  def new

    respond_to do |format|
      format.html # new.html.erb
      format.json  { render :json => @invite }
    end
  end

  # GET /parties/invites/1/edit
  def edit
  end

  # POST /parties/invites
  # POST /parties/invites.json
  def create

    @invite.party = Party.find(params[:party_id])
    @invite.status = "no_answer"

    respond_to do |format|
      if @invite.save

        unless @invite.email.blank?
          PartyMailer.invited(@invite).deliver
          format.html { redirect_back fallback_location: root_path }
          format.json  { render :json => @invite, :status => :created, :location => @invite }
        else
          format.html { redirect_to party_invite_path(@invite.party, @invite) }
          format.json  { render :json => @invite, :status => :created, :location => @invite }
        end
      else
        format.html { render :action => "new" }
        format.json  { render :json => @invite.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /parties/invites/1
  # PUT /parties/invites/1.json
  def update

    respond_to do |format|
      if @invite.update_attributes(invite_params)
        logger.info @invite.inspect
        format.html { redirect_back fallback_location: root_path, notice: 'Invite was successfully updated.' }
        format.json  { head :ok }
      else
        format.html { render :action => "edit" }
        format.json  { render :json => @invite.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /parties/invites/1
  # DELETE /parties/invites/1.json
  def destroy
    @invite.destroy

    respond_to do |format|
      format.html { redirect_to(parties_invites_url) }
      format.json  { head :ok }
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  private
  def invite_params
    params[:invite].permit!
  end

end
