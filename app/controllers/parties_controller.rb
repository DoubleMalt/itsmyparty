class PartiesController < ApplicationController
  load_and_authorize_resource

  # GET /parties
  # GET /parties.json
  def index
    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /parties/1
  # GET /parties/1.json
  def show
    respond_to do |format|
      if params[:key]
        redirect_to party_invite_path(@party, Invite.find_by(invite_key: params[:key], party: @party))
      else
        format.html { render action: 'show' }
      end
    end
  end

  # GET /parties/new
  # GET /parties/new.json
  def new

    respond_to do |format|
      format.html # new.html.erb
      format.json  { render :json => @party }
    end
  end

  # GET /parties/1/edit
  def edit
  end

  # POST /parties
  # POST /parties.json
  def create
    respond_to do |format|
      if @party.save
        invite = Invite.new
        invite.email = @party.creator_mail
        invite.status = "yes"
        invite.role = "admin"
        invite.name = @party.creator_name
        invite.party = @party
        invite.inviter = @party.creator_mail
        invite.save

        @party.reload

        PartyMailer.created(@party).deliver  #party_created @party

        format.html { render action: 'created' }
        format.json  { render :status => :created }
      else
        format.html { render :action => "new" }
        format.json  { render :json => @party.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /parties/1
  # PUT /parties/1.json
  def update
    respond_to do |format|
      if @party.update_attributes(party_params)
        flash[:notice] = 'Party was successfully updated.'
        format.html { redirect_to party_invite_path(@party, invite, key: invite.invite_key) }
        format.json  { head :ok }
      else
        format.html { render :action => "edit" }
        format.json  { render :json => @party.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /parties/1
  # DELETE /parties/1.json
  def destroy
    @party = Party.find(params[:id])
    @party.destroy

    respond_to do |format|
      format.html { redirect_to(parties_url) }
      format.json  { head :ok }
    end
  end

  def rsvp
    @invite = Invite.create(invite_params)
    @invite.party = @party
    @invite.status = 'yes'
    @invite.role = 'guest'
    @invite.custom_invitation = "Hello #{@invite.name}! \n\n Thanks for joining #{@party.topic}!"
    @invite.save!
    PartyMailer.invited(@invite).deliver
    redirect_to party_invite_path(@party, @invite, key: @invite.invite_key)
  end

  private

  def invite_params
    params[:invite].permit([:name, :email])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def party_params
    params[:party].permit([:banner, :topic, :creator_name, :creator_mail, :location, :start, :end, :description, :default_invitation])
  end

end
