class PicturesController < ApplicationController
  load_and_authorize_resource :party
  load_and_authorize_resource :invite, :through => :party
  load_and_authorize_resource :picture, :through => :invite

  def create
    @picture.invite = @invite

    respond_to do |format|
      if @picture.save
        format.html { redirect_to party_invite_path(@invite.party, @invite) }
        format.json  { render :json => @picture, :status => :created, :location => @invite }
      else
        format.html { render :action => "new" }
        format.json  { render :json => @picture.errors, :status => :unprocessable_entity }
      end
    end
  end

  private

  def picture_params
    params[:picture].permit(:picture)
  end
end
