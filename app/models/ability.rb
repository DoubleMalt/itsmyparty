class Ability
  include CanCan::Ability

  def initialize(key)
    can :create, Party
    can :create, Comment do |comment|
      comment.invite.is_invite_key?(key)
    end
    can :create, Picture do |comment|
      comment.invite.is_invite_key?(key)
    end
    can :create, Invite do |invite|
      invite.party.is_admin_key?(key) || (invite.party.open && invite.role == 'guest')
    end
    can :manage, Party do |party|
      party.is_admin_key?(key)
    end
    cannot :destroy, Party
    can :read, Party do |party|
      party.open || party.invites.any? do |invite|
        invite.is_invite_key?(key)
      end
    end
    can :rsvp, Party do |party|
      party.open
    end

    can [:read, :update], Invite do |invite|
      invite.is_invite_key?(key) || invite.party.is_admin_key?(key)
    end

    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user permission to do.
    # If you pass :manage it will apply to every action. Other common actions here are
    # :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. If you pass
    # :all it will apply to every resource. Otherwise pass a Ruby class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details: https://github.com/ryanb/cancan/wiki/Defining-Abilities
  end
end
