class Invite < ActiveRecord::Base

  scope :yes, -> {where(status: 'yes')}
  scope :no, -> {where(status: 'no')}
  scope :maybe, -> {where(status: 'maybe')}
  scope :no_answer, -> {where(status: 'no_answer')}

  belongs_to :party
  has_many :comments
  has_many :pictures

  before_validation :create_key_if_necessary

  def invitation_text
    self.custom_invitation.blank? ? "Hello #{self.name}! \n\n #{self.party.invitation_text}" : self.custom_invitation
  end


  def is_invite_key?(key)
    key == invite_key
  end

  private

  def create_key_if_necessary
    self.invite_key ||= SecureRandom.base58(24)
  end
end
