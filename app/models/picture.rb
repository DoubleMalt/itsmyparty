class Picture < ApplicationRecord
  belongs_to :invite
  has_one_attached :picture
end
