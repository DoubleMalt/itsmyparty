class Party < ActiveRecord::Base

  has_many :invites
  has_many :comments, through: :invites
  has_many :pictures, through: :invites
  has_one_attached :banner

  def is_admin_key?(key)
    invites.any? { |invite| invite.invite_key == key && invite.role == "admin" }
  end

  def may_invite?(key)
    is_admin_key?(key)
  end

  def may_edit?(key)
    is_admin_key?(key)
  end

  def may_see?(key)
    invites.any?{ |invite| invite.is_invite_key?(key) }
  end

  def invitation_text
    self.default_invitation.blank? ? "You have been invited to #{self.topic}!" : self.default_invitation
  end
end
