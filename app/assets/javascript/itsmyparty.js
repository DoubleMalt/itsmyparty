$(document).ready(() => {
  $('a[data-toggle=collapse]').click((e) => {
    $($(e.currentTarget).attr('href')).toggleClass('show');
  });
});
