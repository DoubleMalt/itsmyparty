module ApplicationHelper
  def markdown(source)
    Redcarpet::Markdown.new(
      Redcarpet::Render::HTML,
      autolink: true,
      tables: true).render(source).html_safe
  end
  def title(text)
    content_for :title, text.html_safe
  end
end
