class PartyMailer < ActionMailer::Base

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.party_mailer.invited.subject
  #
  def invited(invite)
    @invite = invite

    mail :to => invite.email, :subject => "You were invited to the Event #{invite.party.topic}"
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.party_mailer.created.subject
  #
  def created(party)
    @party = party

    mail to: party.creator_mail, subject: "#{@party.topic} is on!"
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.party_mailer.answered.subject
  #
  def answered(invite)
    @invite = invite
    mail to: invite.party.creator_mail, subject: "#{invite.name} just answered #{invite.status} to your invitation to for #{@party.topic}."
  end
end
