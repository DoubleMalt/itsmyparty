FROM ruby
LABEL maintainer="christoph@doublemalt.net"

ENV BUNDLE_WITHOUT test,development
ENV RAILS_LOG_TO_STDOUT true
ENV RAILS_SERVE_STATIC_FILES true
ENV RAILS_ENV production


WORKDIR /app

# Install gems
ADD Gemfile* /app/
RUN bundle install
# Install yarn packages
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get -y install yarn
COPY package.json yarn.lock /app/
RUN yarn

# Add the Rails app
ADD . /app

WORKDIR /app

# Expose Puma port
EXPOSE 3000

# Start up
CMD bundle exec rails assets:precompile && bundle exec rails db:prepare && bundle exec rails s
