# It's my party

## How to use it

Go to the [hosted version](https://itsmyparty.doublemalt.net/) or host it
yourself.

## How to host it yourself

### Docker Compose

The included `docker-compose.yml` gives an example how to host it yourself. Just
replace `itsmyparty.localhost` with the hostname you want the service to be
hosted at and adapt `itsmyparty.env.example`.

### Heroku

TBD


## How to contribute

### Suggestions, Questions and Bugs

- Create an issue [here](https://gitlab.com/DoubleMalt/itsmyparty/issues/new)

### Run and develop locally

- Install [Ruby](https://www.ruby-lang.org/) and [Bundler](https://bundler.io/)
- Clone repository
- Go to source directory
- Copy `itsmyparty.env.example` to `.env`
- Run `bundle install && bundle exec rails db:migrate && bundle exec rails s`
