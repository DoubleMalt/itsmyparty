Itsmyparty::Application.routes.draw do


  resources :parties do
    member do
      post 'rsvp'
    end
    resources :invites do
      resources :comments, only: :create
      resources :pictures, only: :create
    end
  end


  root :to => "parties#index"

end
